---
title: Overview
metaTitle: Psonoci Overview | Psono Documentation
meta:
  - name: description
    content: Overview of Psonoci.
---

# Psonoci Overview

## Preamble

Psonoci is the psono CI / CD integration utility. The client is curently in beta.

## Overview

API which is used for `psonoci` is sessionless with local decryption (see
[API key overview](/user/api-key/overview.html) for details).
