---
title: V12 Security Configuration
metaTitle: V12 Security Configuration | Psono Documentation
meta:
  - name: description
    content: Security configuration verification requirements
---

# V12 Security Configuration

Security configuration verification requirements

[[toc]]

This section was incorporated into V11 in Application Security Verification Standard 2.0.

