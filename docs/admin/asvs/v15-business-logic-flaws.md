---
title: V15 Business Logic Flaws
metaTitle: V15 Business Logic Flaws | Psono Documentation
meta:
  - name: description
    content: Business logic verification requirements
---

# V15 Business Logic Flaws

Business logic verification requirements

[[toc]]


## ASVS Verification Requirement


| ID  | Detailed Verification Requirement                                                                                                                                                                                                                             | Level 1                 | Level 2    | Level 3     | Since     |
|------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--|---|---|-----|
| 15.1 | Verify the application will only process business logic flows in sequential step order, with all steps being processed in realistic human time, and not process out of order, skipped steps, process steps from another user, or too quickly submitted transactions. |  | x | x | 2.0 |
| 15.2 | Verify the application has business limits and correctly enforces on a per user basis, with configurable alerting and automated reactions to automated or unusual attack.                                                                                            |  | x | x | 2.0 |

### 15.1

Crucial API endpoints enfoce rate limiting, so all requests done in for a human unrealistic time are throttled. The order of flows that require multiple steps (e.g. the login process with 2FA) are enforced through central states (e.g. the login token cannot be used before it has been activated).


### 15.2

Limits are enforced per IP or per session. Alerting should be configured through external software with audit logs.
